Packer template to test ESS DM
==============================

This Packer_ template tests the ESS Development Machine.

Usage
-----

The tests are run automatically by Jenkins
(you can check the Jenkinsfile).

To test locally, run::

    $ packer build test_update.json


.. _Packer: https://www.packer.io
